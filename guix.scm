(define-module (almannotate)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (guix search-paths)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages chromium)
  #:use-module (gnu packages tmux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages lisp)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module ((guix licenses) :prefix license:))

(define-public python-unidecode
  (package
    (name "python-unidecode")
    (version "1.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Unidecode" version))
       (sha256
        (base32 "013mya7ci362iqgy6wc2abvmgl0qvxhdq6cqgzqbq4l9ab0ri636"))))
    (build-system python-build-system)
    (home-page "")
    (synopsis "ASCII transliterations of Unicode text")
    (description "ASCII transliterations of Unicode text")
    (license license:gpl3)))

(define-public python-almannotate
   (package
    (name "almanotate")
    (version "0.0.0")
    (source
     (local-file "." #:recursive? #t))
    (build-system copy-build-system)
    (arguments
     `(
       #:install-plan
       `(("almanotate" "bin/"))))
    (propagated-inputs
     `(("python" ,python)
       ("python-unidecode" ,python-unidecode)
       ("coreutils" ,coreutils)
       ("mupdf" ,mupdf)
       ))
    (synopsis "Web and tor scraper and crawler")
    (description "See documentation")
    (home-page "https://gitlab.com/edouardklein/gendscraper")
    (license license:agpl3+)))
python-almannotate

;(packages->manifest
; (list sbcl))

